extern crate fibonacci;

use fibonacci::Fibonacci;

fn main() {
    let fib = Fibonacci::new();
    let nums: Vec<i64> = fib.take(10).collect();
    println!(
        "Here are the first ten numbers in the Fibonacci sequence: {:?}",
        nums
    );
}

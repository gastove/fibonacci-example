/// This struct contains the state needed to power an infinite
/// [Iterator](std::iter::Iterator); every call to `next` returns the subsequent
/// [Fibonacci number](https://en.wikipedia.org/wiki/Fibonacci_number), starting
/// at 0.
///
/// # Examples
///
/// ```
/// # use fibonacci::Fibonacci;
/// let mut fib = Fibonacci::new();
/// assert_eq!(fib.next(), Some(0));
/// assert_eq!(fib.next(), Some(1));
/// assert_eq!(fib.next(), Some(1));
/// assert_eq!(fib.next(), Some(2));
/// assert_eq!(fib.next(), Some(3));
/// assert_eq!(fib.next(), Some(5));
/// assert_eq!(fib.next(), Some(8));
/// ```
pub struct Fibonacci {
    /// Used internally to store the first of the previous two numbers. If this
    /// field is `None`, no numbers have been fetched from the generator yet,
    /// and 0 should be returned, indicating the start of the sequence.
    first: Option<i64>,
    /// Used internally to store the second of the previous two numbers. If this
    /// field is None, a 1 should be returned.
    second: Option<i64>,
}

impl Fibonacci {
    pub fn new() -> Self {
        Fibonacci {
            first: None,
            second: None,
        }
    }
}

impl std::iter::Iterator for Fibonacci {
    type Item = i64;

    fn next(&mut self) -> Option<Self::Item> {
        match (self.first, self.second) {
            // If first and second are both `None`, the iterator hasn't been
            // used yet, so return 0.
            (None, None) => {
                let ret = Some(0);
                self.first = ret;
                ret
            }
            // If only second is None, it's the second call to the iterator;
            // return 1.
            (Some(_), None) => {
                let ret = Some(1);
                self.second = ret;
                ret
            }
            // We're on the third or greater call to the iterator. We add first
            // and second, then shuffle values:
            // - `second` becomes `first`
            // - the sum becomes `second`
            // - return the sum
            (Some(first), Some(second)) => {
                let ret = Some(first + second);
                self.first = Some(second);
                self.second = ret;
                ret
            }
            // The Rust compiler checks that all matches are exhaustive, so the
            // program wont compile without this case. However, it should also
            // be impossible, so if we hit it: just crash.
            _ => panic!("This should be impossible!"),
        }
    }
}

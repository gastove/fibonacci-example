FROM rust:1.43 as builder

WORKDIR /app
COPY . /app

RUN cargo doc --no-deps

FROM nginx:1.17.10

COPY --from=builder /app/target/doc /www/data/doc

ADD nginx.conf /etc/nginx/conf.d/default.conf

# Doc hosting!

Yes, this says "fibonacci" all over it; it's an example repo to accompany a [blog
post](https://blog.gastove.com/blog/2020-05-30--self-hosting-rust). To use this yourself, you _should_ only need to fix the project name in
`nginx.conf`:

``` nginx
server {
    root /www/data/doc;

    location / {
        index fibonacci/index.html; # <-- This bit, s/fibonacci/your-project-name
    }
}
```

Put the conf file somewhere the Dockerfile can find it, build, and enjoy!
